# Developer Notes

## General good source for info about go libraries

https://github.com/xtaci/kcp-go

There is loads of great stuff in here, we will be using several of these, in this early stage, the CLI utilities are especially helpful

## Reliable UDP

We need a reliable UDP library. It just so happens there is a very nice, production grade one available:

https://github.com/xtaci/kcp-go

This will be the connection mechanism we will use for p2p traffic between all nodes. It will enable users with good home connections to run RPC and seed nodes, even if their connection is at times busy with other traffic, it will remain responsive and not time out and cost so long in delays to renegotiate connections.