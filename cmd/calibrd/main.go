////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//  cmd/calibrd/main.go                                                       //
//                                                                            //
//  The main server application for the Calibrae blockchain forum system      //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//  Credits for creation of this code can be found in the Git log.            //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//  "LICENCE" for redistribution, derivation and use of this code             //
//                                                                            //
//  This is free and unencumbered software released into the public domain.   //
//                                                                            //
//  Anyone  is  free  to  copy,  modify,  publish,  use,  compile,  sell, or  //
//  distribute  this software,  either in source code  form or as a compiled  //
//  binary, for any purpose, commercial or non-commercial, and by any means.  //
//                                                                            //
//  In jurisdictions  that recognize  copyright laws,  the author or authors  //
//  of this software dedicate any and all copyright interest in the software  //
//  to the public  domain.   We make this dedication  for the benefit of the  //
//  public at  large and to  the detriment  of our heirs  and successors. We  //
//  intend  this  dedication  to  be   an  overt  act  of  relinquishment in  //
//  perpetuity  of  all present  and  future  rights to this  software under  //
//  copyright law.                                                            //
//                                                                            //
//  THE  SOFTWARE  IS  PROVIDED  "AS IS",  WITHOUT  WARRANTY  OF  ANY  KIND,  //
//  EXPRESS  OR  IMPLIED,  INCLUDING  BUT NOT LIMITED  TO  THE WARRANTIES OF  //
//  MERCHANTABILITY,   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  //
//  IN  NO EVENT  SHALL THE  AUTHORS BE LIABLE  FOR ANY  CLAIM,   DAMAGES OR  //
//  OTHER  LIABILITY,  WHETHER IN AN ACTION OF CONTRACT,  TORT OR OTHERWISE,  //
//  ARISING FROM,  OUT OF OR IN CONNECTION WITH  THE SOFTWARE  OR THE USE OR  //
//  OTHER DEALINGS IN THE SOFTWARE.                                           //
//                                                                            //
//  For more information, please refer to <http://unlicense.org/>             //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

package main

import (
	"fmt"
	"os"

	"github.com/namsral/flag"
)

func main() {
	// hard coded information about this program
	const appName = "calibrd"
	const appVer = "v0.0.1"
	const defDataDir = "calibrae-data"
	const defCfgFilename = "config"
	// parse commandline options
	version := flag.Bool("v", false, "Show version information")
	dataDir := flag.String("d", defDataDir, "Set calibrd config and data directory")
	flag.DefaultConfigFlagname = defCfgFilename
	config := flag.String("config", *dataDir+"/"+defCfgFilename, "Use configuration file")
	loglevel := flag.String("loglevel", "info", "Enable logging to console: none|info|error")
	// plugins:
	acctByKey := flag.Bool("acctbykey", false, "Enables the Account by Key plugin")
	acctHist := flag.Bool("accthist", false, "Enables the Account History plugin")
	acctStats := flag.Bool("acctstats", false, "Enables the Account Statistics plugin")
	authUtil := flag.Bool("authutil", false, "Enables the Authentication Utilities plugin")
	blockInfo := flag.Bool("blockinfo", false, "Enables the Block Info plugin")
	chainStats := flag.Bool("chainstats", false, "Enables Blockchain Statistics plugin")
	debugNode := flag.Bool("debug", false, "Enables Debug Node plugin")
	delayedNode := flag.Bool("delayed", false, "Enables the Delayed Node plugin")
	follow := flag.Bool("follow", false, "Enables the Follow plugin")
	//mktHist := flag.Bool("mkthist", false, "Enables the Market History plugin")
	//pvtMsg := flag.Bool("pvtmsg", false, "Enables the Private Message plugin")
	rawBlock := flag.Bool("rawblock", false, "Enables the Raw Block plugin")
	tags := flag.Bool("tags", false, "Enables the Tags plugin")
	witness := flag.Bool("witness", true, "Enables the Witness plugin")
	webApp := flag.Bool("webapp", false, "Enable the Web Application")

	// We use a configuration file by default, and create the default one if it
	// does not exist. The config can be overridden by CLI args or environment variables
	if _, err := os.Stat(*dataDir + "/" + defCfgFilename); os.IsNotExist(err) {
		fmt.Println("Creating data directory and writing default configuration file")
		os.Mkdir(*dataDir, 0711)
		cfgFileHandle, err := os.Create(*dataDir + "/" + defCfgFilename)
		check(err)
		cfgFileHandle.WriteString(`###Default calibrd configuration file
#lines commented out are booleans set default false or default values
#lines ending in * are not implemented yet
#lines ending in ** may not need to ever be implemented
#Note that comments may not appear after values (limitation in github.com/namsral/flag)

## Set log level: none|info|error
loglevel info
## Enables Account by Key plugin *
# acctbykey
## Enables Account History plugin *
# accthist 
## Enables Account Statistics plugin *
# acctstats
## Enables Authentication Utilities plugin * 
# authutil
## Enables Block Information plugin **
# blockinfo
## Enables Blockchain Statistics plugin **
# chainstats
## Sets Debug mode *
# debug
## Sets Delayed Node mode **
# delayed
## Enables Raw Block plugin **
# rawblock
## Enables Tags plugin *
# tags
## Enables Witness plugin *
witness
# webapp # Enables Web Application plugin *
`)
	} else {
		// TODO if there already is a config CLI args and environment variables override config file settings, add the logic to do this
		fmt.Println("Configuration file found")
	}

	flag.Parse()

	fmt.Println("Starting Calibrae server daemon")
	fmt.Println("-------------------------------")
	plugins := []bool{*acctByKey, *acctHist, *acctStats, *authUtil, *blockInfo, *chainStats,
		*debugNode, *delayedNode, *follow,
		//*mktHist,
		//*pvtMsg,
		*rawBlock, *tags, *witness, *webApp}

	// print version information
	if *version {
		fmt.Printf("%s version %s\n", appName, appVer)
		return
	}

	fmt.Printf("Using config directory %s\n", *dataDir)
	fmt.Printf("Loading config from %s\n", *config)

	// count enabled plugins
	pluginCount := 0
	for i := 0; i < len(plugins); i++ {
		if plugins[i] {
			pluginCount++
		}
	}
	fmt.Printf("Enabling %d plugins\n", pluginCount)

	// Show user what log level is being used
	if *loglevel != "none" {
		fmt.Printf("Enabling logging at level '%s'\n", *loglevel)
	}
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}
