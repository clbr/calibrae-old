# Calibrae

A total rewrite of the steem blockchain system, reimplemented in Golang. Calibrae builds upon the things in steem that work, and improves elements that have proven susceptible to abuse:

- enforcing bandwidth limits that correspond to human activity, and not so much that it becomes possible to spam transactions into the database using bot scripts.

- refining the reputation system, to eliminate the ability to abuse the voting system, balancing reputation against stake, adding a reputation reduction when users ignore (shun) bad behaving accounts.

- changing the block production schedule to use Cooperative Proof of Work - witnesses win blocks with PoW solutions, and the rest of the witnesses either accept or reject it based on the proportion of blocks being required to not exceed the proportions of votes the witnesses have received.

- RPC nodes serve up the web application directly, and use the beneficiary system to fund the RPC nodes, with mechanisms in place to combat (malicious) alteration of the app enforced by other nodes checking it via Tor, and logging their reports on the chain.

## Getting the sources, the easy way

```
go get gitlab.com/calibr.ae/calibrae
```

## Build instructions

**[See the section below](#recommendations-for-aspiring-developers) for recommendations for aspiring developers, to set up your build environment before trying these.** 

Many golang repositories do not include these basic instructions because they assume developers have already learned them. We do not make this assumption, we want you to get a taste for it first, then you will see why you need to Go learn more.

To learn more about how you develop with Golang, visit [https://golang.org/doc/install](https://golang.org/doc/install)

### Preparing your workspace

It is not simple to find the answer to this simple question out there on teh interwebz, but when you have external dependencies pulled in via github or similar, you can automate getting the dependencies for everything this way:

```
cd $GOPATH/src
go get ./...
```

This automatically scans the import blocks of every go source in your entire workspace, `go get`s all the necessaries, and then you can `go install path/to/cmd/programname` and voila! The `...` means 'recursively scan all sources and all dependencies'.

When you are adding new dependencies, you will need to `go get` in the directory or you can just rerun over your whole workspace again as described above.

### How to build

The following commands will build and install into your $GOPATH/bin folder:

### Calibrae server daemon

```
go install gitlab.com/calibr.ae/calibrae/cmd/calibrd
```

### Calibrae CLI wallet

```
go install gitlab.com/calibr.ae/calibrae/cmd/wallet
```

### Calibrae wallet server daemon

```
go install gitlab.com/calibr.ae/calibrae/cmd/walletd
```

### Tests

#### General tests:

```
go install gitlab.com/calibr.ae/calibrae/cmd/calibrae-tests
```

#### Plugin tests:

```
go install gitlab.com/calibr.ae/calibrae/cmd/calibrae-plugin-tests
```

## Building locally from your copy of the repository

From within the calibrae/ directory, issue the following command to build and install the binary into your $GOPATH/bin folder (see below recommendations for how to put this in your path on linux):

```
go install ./cmd/<commandname>
```

where &lt;commandname&gt; is one of the subfolders of [calibrae/cmd](calibrae/cmd)

## Commandline parameters standard

All commandline executables in this repository have a standard based on the Golang 'flags' package. We use the extended version from here: [https://github.com/namsral/flag](https://github.com/namsral/flag), which adds the transparent ability to use configuration files and environment variables as well, which is more integrated than the base golang standard.

To get the help information for parameters:

```
<commandname> -h
```

Where there is default options using a configuration file and work directory, commandline flags can be used to set parameters that will override parameters in the directories.

# Recommendations for aspiring developers

Because Calibrae is written in Golang, it is simple for anyone to get involved without learning a new operating system, and the development environment is very simple to understand, our other developers will happily help you, if the instructions below are not sufficient to get you started.

Linux, specifically Arch Linux or a derivative, is highly recommended, but all of the tools listed here are available as binaries for all platforms.

## Golang

You can download Go from here: [https://golang.org/dl/](https://golang.org/dl/)

On linux, it is highly recommended you add the following to your shell init script (for most, this will be `$HOME/.bashrc`):

```
export GOPATH=$HOME
export PATH=$HOME/bin:$PATH
export CDPATH=$GOPATH/src/github.com:$GOPATH/src/gitlab.com:$GOPATH/src/github.com
```

The last command for CDPATH means you can now `cd calibr.ae/calibrae` and instantly be in the root of this repository.

***TODO: Add the relevant instructions for windows, MacOS should be the same since it is based on BSD unix***

This will put all the binaries in the bin/ directory of your profile folder, and set it as part of the command search path, meaning you can invoke generated binaries without specifying path. 

The source files that are downloaded by go to build the binaries will be available in the src/ directory of your home folder, in a folder hierarchy based on the server names and group names of the repositories.

## Integrated Development Environment

Visual Studio Code is recommended for developing Calibrae. 

Download it here: [https://code.visualstudio.com/download](https://code.visualstudio.com/download). 

When using the above recommendation for the GOPATH, you also need to tell VSCode to use the same, for some reason it does not read it out of .bashrc, so add this line to your settings (File->Prefecences->Settings)

```
"go.gopath": "<what you set your GOPATH as>"
```

Note that `$HOME` in the configuration will flag an error. Instead, set it to `/home/<username>` which you can get by opening the integrated terminal and entering

```
echo $HOME
```

Doing this will have everything neatly in one place, even if it doesn't seem to be default for Go presently, it really should be.

For Arch linux users ([Antergos Linux](https://antergos.com/) is very easy to install, based on Arch) install pacaur: 

```
sudo pacman -S pacaur
```

then install VSCode from AUR:

```
pacaur -S visual-studio-code
```

## Git Repository Management

[GitKraken](https://code.visualstudio.com/download) is a very easy to use tool for managing git repositories. VSCode has basic Git functions built in, but for when more complex tasks are required, GitKraken lets you do it with a mouse.