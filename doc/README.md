# Calibrae

## About Calibrae

Calibrae is a total rewrite based on the Steem blockchain system, in Golang, with a completely revised consensus logic aimed at improving its resistance to malicious behaviour including trolling, spamming and scamming, and with a radically simplified ledger aimed at decreasing volatility of the price and promoting a steady increase in the value of the tokens that exceeds its issuance rate, as well as monetising the provision of read access to the database using the beneficiary system to allow any RPC node to serve the web application.
